function openMobileMenu() {
  const menu = document.getElementById('mobileMenu');
  menu.classList.remove('animation-hideDownToRight');
  menu.classList.add('animation-slideUpFromRight');
}
function closeMobileMenu() {
  const menu = document.getElementById('mobileMenu');
  menu.classList.remove('animation-slideUpFromRight');
  menu.classList.add('animation-hideDownToRight');
}
